<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'ballet_schule');

/** MySQL database username */
define('DB_USER', 'admin');

/** MySQL database password */
define('DB_PASSWORD', '123');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY', 'FM9>e.#N?f(z7pi|2zR~PeHU>nZf*oEY$@+3n KD[AXK-e`7PDLow_5(w`.G8c8r');
define('SECURE_AUTH_KEY', 'Vw)ctUz!wsC[AP>**LG)y%9s[S!t!$E&4X*VsQ3]yh4xv-w,IOzHfuPKN_UdUS>7');
define('LOGGED_IN_KEY', '>,ibb=T|2<C(eq,);h><)[ii<78c@$aK.gR/{P3IO7M#AUqxz[!|>mq=,uM^@M}+');
define('NONCE_KEY', 'oEybKu!m[*k^5^Y{W:Wa=|7q0TNQnyd#,hFgU%~l|`5h?ak)Wuyj9Ubwz&n(1:sF');
define('AUTH_SALT', 'T;yZ{{6|!?U?(+di=h @=XrQHMq_H|?,Vli1KR1iD,/{abxZDYf/kn_,,wH-BbD(');
define('SECURE_AUTH_SALT', 'd~sd7biO=o{_OhJ]&7VSoW?(dZ@BI--+Jq![!P#Zgk?isa=zKJ&F.NA./:xEqUr#');
define('LOGGED_IN_SALT', '|FXaa:El|<xHlfIGB/I,GWZ(<E^.x.na5Oon:1aLZ)f^-9Y=8FR/;N|S6H!M+/s^');
define('NONCE_SALT', 'RmEhZEb~r|x1Psu >|n,|qQ|d?Ns74&ALXV0OwKm;BVPVQ|g;4Y<6gE &A(+QyGb');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);
define('WPCF7_AUTOP', false);
define('ALLOW_UNFILTERED_UPLOADS', true);


/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if (!defined('ABSPATH')) {
    define('ABSPATH', dirname(__FILE__) . '/');
}

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
