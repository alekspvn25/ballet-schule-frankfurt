<?php
// Template Name: Home Page

$bottom_text = get_field('bottom_page_text');
$background_image = get_field('background_image');
$site_logo = get_field('site_logo','options');
get_header(); ?>
    <section class="home" <?= ($background_image) ? 'style="background-image: url('.$background_image['sizes']['page_bg'].');"' : ''; ?>>
        <?php if ($site_logo): ?>
            <div class="site-branding home__brand">
                <img src="<?= $site_logo['url'];?>" alt="<?= $site_logo['alt'];?>" />
            </div><!-- .site-branding -->
        <?php endif; ?>
        <div class="home__schools"></div>
        <?php if ($bottom_text): ?>
            <div class="home__bottom-text"><?= $bottom_text;?></div>
        <?php endif; ?>
    </section>
<?php get_footer();
?>