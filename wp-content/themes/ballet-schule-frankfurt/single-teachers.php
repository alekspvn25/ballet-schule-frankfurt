<?php
get_header();
?>
    <section class="hero">
        <div class="inner-container">
            <?php if (have_posts()):
                while (have_posts()) : the_post(); ?>
                    <div class="hero__content teacher-content row">
                        <div class="col-lg-5">
                            <div class="hero__content-teacher-photo"><?= '<img src="' . get_the_post_thumbnail_url() . '" alt="" />'; ?></div>
                        </div>
                        <div class="col-lg-7">
                            <h1 class="hero__content-title"><?php the_title(); ?></h1>
                            <div class="hero__content-descr">
                                <?php the_content(); ?>
                            </div>
                        </div>
                    </div>
                <?php endwhile;
            endif;
            // ?>
        </div>
    </section>
<?php get_template_part('/template-parts/partners'); ?>

<?php get_template_part('/template-parts/flexible-content/teachers'); ?>
<?php get_template_part('/template-parts/flexible-content/subscribe_form'); ?>
<?php get_footer();