<?php
$teachers_args = [
    'post_type' => 'teachers',
    'posts_per_page' => 100,
    'post_status' => 'publish'
];
$teachers_posts = get_posts($teachers_args);
?>
<?php if ($teachers_posts): ?>
    <section class="teachers">
        <div class="inner-container">
            <h3 class="teachers__title">Pädagogen</h3>
            <div class="row">
                <?php foreach ($teachers_posts as $post):
                    $id = $post->ID;
                    setup_postdata($id);
                    $post_thumbnail = get_field('teacher_thumbnail_photo', $id);
                    ?>
                    <a href="<?= get_the_permalink($id); ?>" class="col-lg-3 col-md-6 col-sm-6 teachers__post">
                        <div class="teachers__post-photo">
                            <img src="<?= $post_thumbnail['url']; ?>" alt="<?= $post_thumbnail['alt']; ?>"/>
                        </div>
                        <div class="teachers__post-title"><?php the_title(); ?></div>
                    </a>
                    <?php wp_reset_postdata(); endforeach; ?>
            </div>
        </div>
    </section>
<?php endif;
