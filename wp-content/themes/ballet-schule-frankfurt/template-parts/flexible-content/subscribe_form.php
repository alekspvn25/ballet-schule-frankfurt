<?php
?>
<section class="subscribe">
    <div class="row">
        <div class="subscribe__title"><h3>Hier können sich unsere Mitglieder einloggen!</h3></div>
        <div class="subscribe__form"><?= do_shortcode('[contact-form-7 id="77" title="Subscribe"]'); ?></div>
    </div>
</section>
