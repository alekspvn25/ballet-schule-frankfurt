<?php
$partners = get_field('partners', 'options');
$partners_title = get_field('title_partners', 'options');
if ($partners): ?>
    <section class="partners">
        <?php if ($partners_title): ?>
            <div class="partners__title"><?= $partners_title; ?></div>
        <?php endif; ?>
        <div class="row">
            <?php foreach ($partners as $partner): ?>
                <div class="col-lg-3 col-md-6 col-sm-6 d-flex justify-content-center">
                    <a class="partners__logo"
                       <?= ($partner['link']) ? 'href="' . $partner['link'] . '" target="_blank"' : ''; ?>>
                        <img src="<?= $partner['logo_image']['url']; ?>" alt="<?= $partner['logo_image']['alt']; ?>"/>
                    </a>
                </div>
            <?php endforeach; ?>
        </div>
    </section>
<?php endif; ?>