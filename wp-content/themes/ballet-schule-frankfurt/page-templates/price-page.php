<?php
//Template Name: Price page
$price_bg_hero = get_field('slider_top_price');
$price_title_hero = get_field('hero_price_title');

get_header(); ?>
    <section class="hero page-temp">
        <div class="inner-container">
            <div class="hero__content price-content">
                <a class="hero__content-nav" href="#"><?= __('Neu', THEME_TD); ?></a>
                <?php if ($price_title_hero): ?>
                    <div class="hero__content-title sep"><?= $price_title_hero; ?></div>
                <?php endif; ?>
            </div>

        </div>
        <?php if ($price_bg_hero): ?>
            <div class="hero__image">
                <div class="hero__slider">
                    <?php foreach ($price_bg_hero as $image): ?>
                        <img src="<?= $image['sizes']['page_bg']; ?>" alt="<?= $image['alt']; ?>"/>
                    <?php endforeach; ?>
                </div>
            </div>
        <?php endif; ?>
    </section>

<?php get_template_part('/template-parts/partners'); ?>

<?php get_footer();