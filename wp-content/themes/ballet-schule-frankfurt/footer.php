<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package ballet-schule-frankfurt
 */
$fields = get_fields('options');
?>

</div><!-- #content -->

<footer class="footer">
    <div class="inner-container">
        <div class="row">
            <?php if ($fields['secondary_logo']): ?>
                <div class="col-lg-3 col-md-6 col-sm-6 col-xs-12">
                    <div class="footer__logo">
                        <a href="<?= get_home_url(); ?>">
                            <img src="<?= $fields['secondary_logo']['url']; ?>"
                                 alt="<?= $fields['secondary_logo']['alt']; ?>"/>
                        </a>
                    </div>
                </div>
            <?php endif; ?>
            <div class="col-lg-3 col-md-6 col-sm-6 col-xs-12">
                <ul class="footer__schools">
                    <li><a href="">Frankfurt-Bornheim</a></li>
                    <li><a href="">Frankfurt-Bockenheim</a></li>
                    <li><a href="">Offenbach-Stadtmitte</a></li>
                    <li><a href="">Bad Vilbel-Massenheim</a></li>
                </ul>
            </div>
            <?php if ($fields['footer_info']): ?>
                <div class="col-lg-3 col-md-6 col-sm-6 col-xs-12">
                    <div class="footer__info"><?= $fields['footer_info']; ?></div>
                </div>
            <?php endif; ?>
            <?php if ($fields['social_media']): ?>
                <div class="col-lg-3 col-md-6 col-sm-6 col-xs-12">
                    <ul class="footer__social">
                        <?php foreach ($fields['social_media'] as $item): ?>
                            <li class="footer__social-item">
                                <a href="<?= $item['link']; ?>"><img src="<?= $item['social_logo']['url']; ?>"
                                                                     alt="<?= $item['social_logo']['alt']; ?>"></a>
                            </li>
                        <?php endforeach; ?>
                    </ul>
                </div>
            <?php endif; ?>
        </div>
    </div>
</footer><!-- #colophon -->

<?php wp_footer(); ?>

</body>
</html>
